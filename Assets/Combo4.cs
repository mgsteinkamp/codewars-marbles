 using UnityEngine;
 using System.Collections;
 using System.IO;
 public class Combo4 : MonoBehaviour
 {
	GUIContent[] comboBoxList;
	private ComboBox comboBoxControl = new ComboBox();
	private GUIStyle listStyle = new GUIStyle();

	private void Start()
	{
	    comboBoxList = new GUIContent[4];
	    comboBoxList[0] = new GUIContent("1");
		comboBoxList[1] = new GUIContent("3");
		comboBoxList[2] = new GUIContent("5");
		comboBoxList[3] = new GUIContent("10");


	    listStyle.normal.textColor = Color.white; 
	    listStyle.onHover.background =
	    listStyle.hover.background = new Texture2D(2, 2);
	    listStyle.padding.left = 1;
	    listStyle.padding.right = 
	    listStyle.padding.top = 1;
	    listStyle.padding.bottom = 4;
	}

	private void OnGUI () 
	{
	    int selectedItemIndex = comboBoxControl.GetSelectedItemIndex();
	    selectedItemIndex = comboBoxControl.List( 
			new Rect(5, 430, 120, 20), comboBoxList[selectedItemIndex].text, comboBoxList, listStyle );
        GUI.Label( new Rect(5, 410, 100, 20), 
			"Player 2 Weight");
	}
	
	private void Update()
	{
		int select = comboBoxControl.GetSelectedItemIndex();
		if (select == 0)
			GameObject.Find("Sphere2").rigidbody.mass = 1;
		if (select == 1)
			GameObject.Find("Sphere2").rigidbody.mass = 3;
		if (select == 2)
			GameObject.Find("Sphere2").rigidbody.mass = 5;
		if (select == 3)
			GameObject.Find("Sphere2").rigidbody.mass = 10;
		
	}
 }